import java.util.Arrays;
import java.util.Scanner;

public class ArrayManipulation {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "Alice", "Bob", "Charlie", "David" };
        double[] values = new double[4];
        String[] reversedNames = { "", "", "", "" };
        int numall = 0;
        double max = 0;
        // Sum the numbers
        System.out.println("Sum the numbers");
        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);
            numall = numall + numbers[i];
        }
        // Print the names
        System.out.println("print names");
        for (String i : names) {
            System.out.println(i);
        }
        // Read values and find the maximum
        System.out.println("Read values and find the maximum");
        for (int i = 0; i < 4; i++) {
            values[i] = kb.nextDouble();
            if (max < values[i]) {
                max = values[i];
            }
        }
        System.out.println(numall);
        System.out.println(max);
        // Reverse the names
        System.out.println("Reverse the names");
        for (int i = 0; i < 4; i++) {
            reversedNames[i] = names[3 - i];
            System.out.println(reversedNames[i]);
        }
        // Sort and print the numbers
        System.out.println("Sort and print the numbers");
        Arrays.sort(numbers);
        for (int i = 0; i < 5; i++) {
            System.out.print(numbers[i] + " ");
        }
        int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };
        duplicateZeros(arr);

        System.out.println("\nArray with duplicated zeros:");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        int[] nums1 = { 1, 2, 3, 0, 0, 0 };
        int m = 3;
        int[] nums2 = { 2, 5, 6 };
        int n = 3;
        merge(nums1, m, nums2, n);
        System.out.println("\nmerge:");
        for (int num : nums1) {
            System.out.print(num + " ");
        }
    }

    private static int[] duplicateZeros(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];
                }
                arr[i + 1] = 0;
                i++;
            }
        }
        return arr;
    }

    public static int[] merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1, j = n - 1, k = m + n - 1;
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[k--] = nums1[i--];
            } else {
                nums1[k--] = nums2[j--];
            }
        }
        while (j >= 0) {
            nums1[k--] = nums2[j--];
        }
        return nums1;
    }
}
